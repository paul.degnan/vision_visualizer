var vision_data;
var canvasObj;
var canvasElem;
var context;
var canvasHeight;
var canvasWidth;
var currentImg;
var img = new Image();

function featureTabToggle( evt ){

	if( $( evt.toElement ).hasClass( "selected" ) ){

		$( evt.toElement ).addClass( "unselected" );
		$( evt.toElement ).removeClass( "selected" );

	} else {

		$( evt.toElement ).addClass( "selected" );
		$( evt.toElement ).removeClass( "unselected" );

	}

	showData( vision_data );

}

function showData( data ){

	//clearing out anything previously shown: both the canvas and any display divs
	context.clearRect( 0, 0, canvasWidth, canvasHeight );
	context.drawImage( currentImg, 0, 0 );
	$( '#image_properties' ).html( "" );
	$( '#label_annotations' ).html( "" );
	$( '#landmark_annotations' ).html( "" );
	$( '#safesearch_annotations' ).html( "" );
	$( '.error' ).html( "" );

	//first show all tabs...
	// $( ".feature_tab" ).show();

	//then hide any that actually don't have data
	if ( ! data.faceAnnotations ){ $( "#FACE_DETECTION" ).hide(); } else { $( "#FACE_DETECTION" ).show(); }
	if ( ! data.logoAnnotations ){ $( "#LOGO_DETECTION" ).hide(); } else { $( "#LOGO_DETECTION" ).show(); }
	if ( ! data.labelAnnotations ){ $( "#LABEL_DETECTION" ).hide(); } else { $( "#LABEL_DETECTION" ).show(); }
	if ( ! data.textAnnotations ){ $( "#TEXT_DETECTION" ).hide(); } else { $( "#TEXT_DETECTION" ).show(); }
	if ( ! data.imagePropertiesAnnotation ){ $( "#IMAGE_PROPERTIES" ).hide(); } else { $( "#IMAGE_PROPERTIES" ).show(); }
	if ( ! data.safeSearchAnnotation ){ $( "#SAFE_SEARCH_DETECTION" ).hide(); } else { $( "#SAFE_SEARCH_DETECTION" ).show(); }
	if ( ! data.landmarkAnnotations ){ $( "#LANDMARK_DETECTION" ).hide(); } else { $( "#LANDMARK_DETECTION" ).show(); }

	if ( data.faceAnnotations && $( "#FACE_DETECTION" ).hasClass( "selected" ) ){

		showFaceData( data.faceAnnotations );

	}

	if ( data.logoAnnotations && $( "#LOGO_DETECTION" ).hasClass( "selected" ) ){

		showLogoData( data.logoAnnotations );

	}

	if ( data.labelAnnotations && $( "#LABEL_DETECTION" ).hasClass( "selected" ) ){

		showLabelData( data.labelAnnotations );
		
	}

	if ( data.textAnnotations && $( "#TEXT_DETECTION" ).hasClass( "selected" ) ){

		showTextData( data.textAnnotations );

	}

	if ( data.imagePropertiesAnnotation && $( "#IMAGE_PROPERTIES" ).hasClass( "selected" ) ){

		showImagePropertiesData( data.imagePropertiesAnnotation );

	}

	if ( data.landmarkAnnotations && $( "#LANDMARK_DETECTION" ).hasClass( "selected" ) ){

		showLandmarkData( data.landmarkAnnotations );

	}

	if ( data.safeSearchAnnotation && $( "#SAFE_SEARCH_DETECTION" ).hasClass( "selected" ) ){

		showSafesearchData( data.safeSearchAnnotation );

	}
}	

function showLabelData( data ){

	var divHtml = [];

	for( var i = 0; i < data.length; i++ ){

		divHtml.push( 
			"description: " + data[ i ].description + "<br>" +
			"mid: " + data[ i ].mid + "<br>" +
			"score: " + data[ i ].score 
		);

	}

	$( "#label_annotations" ).append( divHtml.join( "<br><br>" ) );

}

function showSafesearchData( data ){

	var divHtml = [];

	for( var key in data ) {
    
        if ( data.hasOwnProperty( key ) ) {
    	
			divHtml.push( key + ": " + data[ key ] );

        }
    }


	$( "#safesearch_annotations" ).append( divHtml.join( "<br><br>" ) );

}

function showImagePropertiesData( data ){

	var colors = data.dominantColors.colors;

	for ( var i = 0; i < colors.length; i++ ){

		var divHtml = "<div style='background-color:rgb( " + colors[ i ].color.red + ", " + colors[ i ].color.green + ", " + colors[ i ].color.blue + " )'></div>"

		$( '#image_properties' ).append( divHtml );

	}

}
function showLandmarkData( data ){

	var divHtml = [];
    
	for ( var i = 0; i < data.length; i++ ){

		drawBoxFromBoundingVertices( data[ i ].boundingPoly.vertices, { "r": "255", "g": "85", "b": "0" }, { "r": "255", "g": "170", "b": "128" } );

		divHtml.push( 
			"description: " + data[ i ].description 
		);

	}

	$( "#landmark_annotations" ).append( divHtml.join( "<br><br>" ) );

}

function showLogoData( data ){

	for ( var i = 0; i < data.length; i++ ){

		drawBoxFromBoundingVertices( data[ i ].boundingPoly.vertices, { "r": "0", "g": "0", "b": "255" }, { "r": "204", "g": "204", "b": "255" } );

	}

}

function showTextData( data ){

	var border = { "r": "0", "g": "255", "b": "0" };
	var fill = { "r": "204", "g": "255", "b": "204" };

	for ( var i = 0; i < data.length; i++ ){

		var vertices = data[ i ].boundingPoly.vertices;

		context.beginPath();

		context.strokeStyle = "rgba( " + border.r + ", " + border.g + ", " + border.b + ", 0.5)";
		context.fillStyle = "rgba( " + fill.r + ", " + fill.g + ", " + fill.b + ", 0.5)";

		context.moveTo( vertices[ 0 ].x, vertices[ 0 ].y );
		context.lineTo( vertices[ 1 ].x, vertices[ 1 ].y );
		context.lineTo( vertices[ 2 ].x, vertices[ 2 ].y );
		context.lineTo( vertices[ 3 ].x, vertices[ 3 ].y );
		context.lineTo( vertices[ 0 ].x, vertices[ 0 ].y );

		context.fill();
		context.stroke();

		context.closePath();

	}

}

function showFaceData( data ){

	var border = { 
		"r": "255", 
		"g": "0", 
		"b": "0" 
	};

	for ( var i = 0; i < data.length; i++ ){

		drawBoxFromBoundingVertices( data[ i ].boundingPoly.vertices, border, { "r": "255", "g": "204", "b": "204" } );

		var landmarks = data[ i ].landmarks;
		for ( var j = 0; j < landmarks.length; j++ ){

			context.fillStyle = "rgba( 255, 255, 255, 1.0)";
			context.fillRect( landmarks[ j ].position.x-2, landmarks[ j ].position.y-2, 5, 5 );
			context.strokeStyle = "rgba( " + border.r + ", " + border.g + ", " + border.b + ", 1.0)";
			context.strokeRect( landmarks[ j ].position.x-2, landmarks[ j ].position.y-2, 5, 5 );

		}

	}

}

function drawBoxFromBoundingVertices( vertices, border, fill ){

	var x = vertices[ 0 ].x;
	var y = vertices[ 0 ].y;

	var width = vertices[ 1 ].x - vertices[ 0 ].x
	var height = vertices[ 2 ].y - vertices[ 0 ].y
	
	context.fillStyle = "rgba( " + fill.r + ", " + fill.g + ", " + fill.b + ", 0.5)";
	context.fillRect( x, y, width, height );
	context.strokeStyle = "rgba( " + border.r + ", " + border.g + ", " + border.b + ", 0.5)";
	context.strokeRect( x, y, width, height );


}

$( document ).ready( function(){

	canvasObj = $( "#comicpage" );
	canvasElem = canvasObj[ 0 ];
	context = canvasElem.getContext( "2d" );

	//default off; will be shown according to what features are returned
	// $( ".feature_tab" ).hide();

	$( ".feature_tab" ).on( "click", featureTabToggle );


	// function loadImg( img ){

	// 	$( img ).load( function(){ 

	// 		canvasWidth = this.width;
	// 		canvasHeight = this.height;

	// 		canvasObj.attr( 'width', canvasWidth );
	// 		canvasObj.attr( 'height', canvasHeight );

	// 		$.ajax({
	// 			url: json
	// 		}).done( function( data ) {
				
	// 			console.log( data );

	// 			vision_data = data.responses[ 0 ];
	// 			showData( vision_data )

	// 		}).error( function( data, textStatus, jqXHR ){ 

	// 			console.log( jqXHR.getAllResponseHeaders() );

	// 		});

	// 	});

	// }

	// var img = new Image();   
	// img.src = '2_a0ef.jpg';
	// currentImg = img;

	// loadImg( img, "p2.json" );

	function uiValidations(){

		var result = { "success": true, "errorDescription": "" };

		if( $( '#key' ).val() == '' ){

			result.success = false;
			result.errorDescription = "This won't work without a Cloud Vision API key. Get one <a href='https://cloud.google.com/vision/docs/quickstart' target='_blank'>here</a>"

		} else if( ( $( "#url" ).val() == "" ) && ( $( "#b64" ).val() == "" ) ){

			result.success = false;
			result.errorDescription = "Either the URL of an image or the base64 content of an image must be entered."

		} else if ( ( $( "#url" ).val() != "" ) && ( $( "#b64" ).val() != "" ) ) {

			result.success = false;
			result.errorDescription = "I am confused. Do you want to enter a URL or base64 encoded image? Please enter only one."

		} else if ( ( $( "#b64" ).val() != "" ) && ( $( "#imgType" ).val() == "image format" ) ) {

			result.success = false;
			result.errorDescription = "For base64 requests, you must also enter the type of the original image."

		}

		return result;

	}

	$( "#goButton" ).on( "click", function(){
		
		var validationResult = uiValidations();

		if ( ! validationResult.success ){

			$( ".error" ).html( validationResult.errorDescription );

		} else {

			var b64 = $( "#b64" ).val();
			var url = $( "#url" ).val();

			executeLookup( { "b64": b64, "url": url } );

		}

	} );


	function executeLookup( urlData ){

		//TODO: stackoverflow ref when online
		function htmlEncode( value ){

			return $( '<div/>' ).text( value ).html();

		}

		//uiValidation ensures only b64 or url is filled in
		var fullUrl = ( urlData.url != '' )?  urlData.url: 'data:image/' + $( '#imgType' ).val() + ';base64, ' + encodeURIComponent( urlData.b64 );
		// var fullUrl = ( urlData.url != '' )?  urlData.url: encodeURIComponent( urlData.b64 );

		var img = new Image();   
		img.src = fullUrl;

		currentImg = img;

		// $( img ).click( function(){ 
		$( img ).load( function( data ){ 

			canvasWidth = this.width;
			canvasHeight = this.height;

			canvasObj.attr( 'width', canvasWidth );
			canvasObj.attr( 'height', canvasHeight );

			context.clearRect( 0, 0, canvasWidth, canvasHeight );
			context.drawImage( currentImg, 0, 0 );

			var b64;

			//no need to take this step if the user has already submitted a dataURL
			if ( urlData.b64 == '' ){

				var dataURL = canvasElem.toDataURL();
				b64 = dataURL.substring( 22 ); //stripping off dataURL header: "data:image/png;base64,"

			} else {

				b64 = urlData.b64;
				// $( "#imgoutput" ).html( encodeURIComponent( b64 ) );

			}


			// var requestJson = '{"requests":[{"image":{"source": {"gcsImageUri": "gs://cloudvision_experiments/IMG_4292.jpg"}},"features":[ ' + 
			var requestJson = '{"requests":[{"image":{"content": "' + b64 + '"},"features":[ ' + 
			'{ "type": "FACE_DETECTION" },' + 
			'{ "type": "LANDMARK_DETECTION" },' + 
			'{ "type": "LOGO_DETECTION" },' + 
			'{ "type": "LABEL_DETECTION" },' + 
			'{ "type": "TEXT_DETECTION" },' + 
			'{ "type": "SAFE_SEARCH_DETECTION" },' + 
			'{ "type": "IMAGE_PROPERTIES" },' + 
			']}]}';

			var apiKey = $( '#key' ).val();

			//assistance from http://stackoverflow.com/questions/23219033/show-a-progress-on-multiple-file-upload-jquery-ajax
			function progress( evt ){

			    if( evt.lengthComputable ){

			        var max = evt.total;
			        var current = evt.loaded;

			        var percentage = current/max;
			        $( "#progressColor" ).width( $( "#progressBar" ).width() * percentage );

			    }  
			 }

	 		$.ajax({

				url: 'https://content-vision.googleapis.com/v1/images:annotate?fields=responses&key=' + apiKey + '&alt=json',
				type: 'post',
				data: requestJson,
				contentType: "application/json",
				//also http://stackoverflow.com/questions/23219033/show-a-progress-on-multiple-file-upload-jquery-ajax
				xhr: function() {
	                var myXhr = $.ajaxSettings.xhr();
	                if(myXhr.upload){
	                    myXhr.upload.addEventListener('progress',progress, false);
	                }
	                return myXhr;
	        	},
	    		beforeSend: function( XMLHttpRequest ){ console.log( "before send" ); }			

			}).done( function( data ) {
				
				console.log( "done" );
				console.log( data );

				if ( data.responses[ 0 ].error ){

					$( '.error' ).html( JSON.stringify( data.responses[ 0 ].error ) );

				} else {

					vision_data = data.responses[ 0 ];
					showData( vision_data )

				}

			}).error( function( data ){ 

				if ( typeof( data.responseJSON ) !== "undefined" ){

					$( '.error' ).html( JSON.stringify( data.responseJSON.error ) );

					if ( canvasHeight * canvasWidth > 1920000 ){

						$( '.error' ).html( $( '.error' ).html() + '\n\nYour image is probably too large. Cloud vision generally doesn\'t accept images larger than 1600 x 1200' );

					}

				} else {

					$( '.error' ).html( JSON.stringify( data ) );

				}

			});

		});

	}


	// $( "#page_choose" ).on( 'change', function(){
	// 	var page_data = this.value.split( "|" );
	
	// 	var changedImg = new Image();   
	// 	changedImg.src = page_data[ 1 ];
	// 	currentImg = changedImg;

	// 	loadImg( changedImg, page_data[ 0 ] );

	// } );

} );

