# Cloud Vision Visualizer

Paul Degnan

This tool is something I originally created to present at the [Google Developer Group NYC machine learning meetup](http://www.meetup.com/gdgnyc/events/230848397/) on 6/6/16. I generalized it a bit and hopefully you'll find it useful if you want to quickly visualize results from the Google Cloud Vision API. (If you'd like to see shaky iPhone video of the talk: https://www.youtube.com/watch?v=1arpnx4rOFM) 

There's a working instance at http://sueandpaul.com/vision_visualizer/viewer.html

### INSTALLATION

Set up the project directory at any webserver you control. All links are relative so it's self-contained. Alternatively, you can just save the files to your desktop and use file --> open to load viewer.html, but if you do this, you'll only be able to run cloud vision requests on base64 encoded files.

### USAGE

- Open /vision_visualizer/viewer.html
- Enter your Google Cloud Vision API key (If you don't have one, follow instructions at https://cloud.google.com/vision/docs/quickstart to create one)
	- If you want to do a request from a URL, use the second field -- NOTE the following
		- You can't use this option if you are running from the filesystem of your local machine: the canvas will not allow export of base64 data.
		- The image will have to either be on your local server, or will need to have a Access-Control-Allow-Origin header set to "*" or the URL of your server. 
	- You can also directly enter the base64 encoded data of the image. (This option works from the local filesytem too.) Just enter the base64 data in the third field and choose the correct file type from the dropdown.

### SAMPLE IMAGES

A collection of sample images is included in the project, which should give you examples of all the features you can detect. Try the included images to see examples of the features you're interested in:

All images will show data from the features: IMAGE_PROPERTIES and SAFE_SEARCH_DETECTION

- photos/barcelona.jpg: LANDMARK_DETECTION
- photos/citifield.jpg: TEXT_DETECTION, LANDMARK_DETECTION 
- photos/gettysburg.jpg: TEXT_DETECTION, LANDMARK_DETECTION 
- photos/google.jpg: LOGO_DETECTION
- photos/IMG_4293.jpg: TEXT_DETECTION, LANDMARK_DETECTION 
- photos/IMG_4293.b64: (same image as above, so you can try base64. slightly lower res)
- photos/obama.jpg: FACE_DETECTION, LOGO_DETECTION (false postive?)
- photos/obama_cabinet.jpg: FACE_DETECTION 

### TROUBLESHOOTING

The most common problem seems to be images that are too large. Images of 1600x1200 seem to work fairly reliably for URL submissions, and base64 images break down at a smaller resolution due to the increased size of the files.